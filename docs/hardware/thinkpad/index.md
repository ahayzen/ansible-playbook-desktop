# Device specific

  * [T480](/docs/hardware/thinkpad/t480.md)

# Battery

Start and stop thresholds are set via TLP (`/etc/tlp.d/99-battery-charge-threshold.conf`), which is set to start at 75% and stop at 80%.

Note that `sudo tlp fullcharge`, `sudo tlp chargeonce`, and `sudo tlp discharge` can be used to override these settings in a session.
