# Nvidia Drivers

On Debian the following is required to install Nvidia drivers.

Firstly, install and run `nvidia-detect`.

```
sudo apt install nvidia-detect
nvidia-detect
```

The output will tell you which driver to install, this is usually `nvidia-driver`, install the driver then restart your machine.

```
sudo apt install nvidia-driver
sudo reboot
```

# Nvidia Docker

Note this is all nvidia-docker2 as Debian buster has an old version of docker (as per https://github.com/NVIDIA/nvidia-docker/wiki/Installation-(version-2.0) ).

Once Debian has docker 19.03 this could be much simpler https://github.com/NVIDIA/nvidia-docker

## Host Install

Add Nvidia repository

```
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update
```

Install nvidia runtime and restart docker

```
sudo apt install nvidia-docker2 libcuda1
sudo pkill -SIGHUP dockerd
```

Check that the following command works

```
nvidia-container-cli info
```

## Container Install

Add to the top of the Dockerfile

```
FROM nvidia/opengl:1.0-glvnd-runtime-ubuntu16.04 as nvidia
```

Note there is also a `nvidia/opengl:1.0-glvnd-runtime-ubuntu18.04` (these are listed here https://hub.docker.com/r/nvidia/opengl/ )

Add to the bottom of the docker file

```
COPY --from=nvidia /usr/local /usr/local
COPY --from=nvidia /etc/ld.so.conf.d/glvnd.conf /etc/ld.so.conf.d/glvnd.conf

ENV NVIDIA_VISIBLE_DEVICES=all NVIDIA_DRIVER_CAPABILITIES=all
```

If you are using Visual Studio Code's remote containers then add to the run args of `devcontainer.json` the following

```
// Use Nvidia runtime
"--runtime=nvidia",
```

Note that currently windows cannot be spawned unless `mesa-utils` is installed at runtime of the container (something seems to be broken).

# Nvidia VSCode

When resuming from suspend the default integrated terminal renderer type of `canvas` causes issues, set the following in your config file to result these issues.

```
"terminal.integrated.rendererType": "dom",
```