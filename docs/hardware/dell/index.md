# Device specific

  * [XPS 9360](/docs/hardware/dell/xps_9360.md)

# Secure Boot

Some Dell XPS devices that ship with Linux pre-installed have secure boot validation disabled even though secure boot is enabled in the BIOS.

When the device boot you'll see "Booting in insecure mode" as Linux starts in the console output.

If your OS supports secure boot this can be resolved by enabling validation.

```bash
$ sudo mokutil --enable-validation
```

This will ask for a temporary password, simply enter `12345678` as this will be asked for on reboot.

After rebooting the device you will enter MOK's UI, which allows you to change the enabled state.
