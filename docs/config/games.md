
# PlayOnLinux

## Force Windowed

If a game doesn't have windowed mode you can force it to use a virtual desktop.

Navigate to `Game -> Configure -> Wine -> Configure Wine -> Graphics` then enable `Emulate a virtual desktop` and set a resolution.

# Steam

## Backup

The following locations could be manually saved as they aren't included in Steam cloud save, look through the sub folders for save game data.

| Game | Location |
|------|----------|
| AOE HD | `~/.steam/steam/steamapps/common/Age2HD` |
| GRID | `~/.local/share/feral-interactive/GRID Autosport` |
| WarThunder | `~/.config/WarThunder` |

## Proton

Any games which have issues when using proton due to vulkan or graphical issue, you can try changing the launch options to the following to force WINE3D OpenGL rendering.

```
PROTON_USE_WINED3D=1 %command%
```
