# Installation

To install language packs and other locale relevant packages run the following command for your language (eg `ja`)

```bash
$ sudo apt install `check-language-support --language=LANG`
$ flatpak config set extra-languages LANG && flatpak update
```

To install common languages (Chinese, German, Italian, Japanese, Spanish).

```bash
$ sudo apt install `check-language-support --language=zh` `check-language-support --language=de` `check-language-support --language=it` `check-language-support --language=ja` `check-language-support --language=es`
$ flatpak config set extra-languages zh,de,it,ja,es && flatpak update
```

# Configuration

## Input

  * For Japanese input open "Language & Region" in settings, then add the "Japanese (Mozc)" input source.
