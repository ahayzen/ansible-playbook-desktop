
The following tasks are not part of the automated playbook and need to be executed manually.

# Config

  * Any [pinning](/docs/development/debian/pinning.md) of packages from backports
  * Optionally enable [dark theme](/docs/config/appearance.md)
  * [Import GPG keys](/docs/data/gpg.md)
  * [Language and Input](/docs/config/locales.md)
  * Wallpaper

## Remote Setup

  * Copy machine ssh public key onto remotes
    * Note the following command can change the SSH key password `$ ssh-keygen -f ~/.ssh/id_rsa -p`

## Sign in

  * Canonical Livepatch
  * Discord
  * Firefox (copy folder?)
  * Google online accounts
  * Hexchat (copy folder?)
  * Spotify
  * Steam
  * Telegram (copy folder?)
  * Zoom

# Data

  * See [Backup and Restore](/docs/data/backup_restore.md) for data to restore

# Hardware Quirks

Check the [hardware](/docs/hardware) for hardware specific tweaks which can provide additional improvements.
