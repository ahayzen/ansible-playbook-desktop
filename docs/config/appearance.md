# Dark Theme

To change to use a dark theme.

  * `gsettings set org.gnome.desktop.interface gtk-theme Adwaita-dark`
  * `mkdir -p ~/.config/environment.d/`
  * `echo "QT_STYLE_OVERRIDE=Adwaita-dark" > ~/.config/environment.d/90adwaita-qt.conf`
  * Restart your session
