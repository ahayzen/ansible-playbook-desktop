
# Audio

The following allows for using pipewire for audio on Ubuntu 22.04 and Debian 12.

```
sudo apt install pipewire-pulse

// Disable and stop the PulseAudio service with:
systemctl --user --now disable pulseaudio.service pulseaudio.socket
// Enable and start the new pipewire-pulse service with:
systemctl --user --now enable pipewire pipewire-pulse

// Prevent pulseaudio from starting after reboot
systemctl --user mask pulseaudio

systemctl reboot
```

For Jack, ALSA, or Debian 11 follow further instructions here https://wiki.debian.org/PipeWire#For_PulseAudio
