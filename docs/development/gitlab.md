# Repository defaults

In Settings -> General -> Merge Requests change the "Merge method" to "Fast-forward merge".

# CI Snippets

Note that some of these snippets could be easily combined into one task.

## Ansible

```
# Ansible check
ansible:
  image: debian:stable-slim
  before_script:
    - apt-get update
    - apt-get install -y --no-install-recommends ansible
  script:
    - ansible-playbook --syntax-check playbook.yml
```

## AppArmor

```
# AppArmor check
apparmor:
  image: debian:stable-slim
  before_script:
    - apt-get update
    - apt-get install -y --no-install-recommends apparmor
  script:
    - apparmor_parser -Q $(grep -IRl "#\(include <tunables/global>\)" --exclude-dir=.git)
```

## Python

```
# Flake 8 check
flake8:
  image: debian:stable-slim
  before_script:
    - apt-get update
    - apt-get install -y --no-install-recommends flake8
  script:
    - flake8 $(grep -IRl "#\!\(/usr/bin/env python3\)" --exclude-dir=.git)
```

## Reuse

```
# Reuse check
reuse:
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - reuse lint
```

## Shellcheck

```
# Shellcheck
shellcheck:
  image: debian:stable-slim
  before_script:
    - apt-get update
    - apt-get install -y --no-install-recommends shellcheck
  script:
    - shellcheck $(grep -IRl "#\!\(/bin/bash\|/bin/sh\|/usr/bin/env bash\|/usr/bin/env sh\)" --exclude-dir=.git)
```
