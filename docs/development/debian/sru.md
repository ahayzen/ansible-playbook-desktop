
# Pull existing sources

Create a folder to work in.

```bash
mkdir -p $PROJECT/$SERIES/$VERSION
cd $PROJECT/$SERIES/$VERSION
```

Get the current source code.

```bash
pull-lp-source $PROJECT $SERIES
cd $PROJECT-$CURRENT_VERSION
```

# Pull new version

Download the latest upstream version.

```bash
uscan --download-version $NEW_VERSION
```

Create a new source to work with.

```bash
uupdate ../$PROJECT-$NEW_VERSION.tar.xz
cd ../$PROJECT-$NEW_VERSION
```

# Update debian files

Update the following files.

debian/control

  * Add XSBC-Original-Maintainer for old maintainer
  * Update debian/control maintainer to have @ubuntu.com address (eg Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>)

debian/changelog

  * Update latest debian/changelog entry to use $SERIES instead of unreleased
  * Update changelog to have better info that "new upstream release"
  * Add a link to the SRU bug "* Update to $NEW_VERSION (LP: #XXXXXXX)"
  * If following debian, check what other changes have happened in their changelog and copy
  * If doing an SRU change the version from ubuntu1 to ubuntu0.1
  * Prefer "debian/copyright: Update" to "d/copyright: Update"

debian/patches

  * Remove patches that aren't required from debian/patches/A.B.C and debian/patches/series

# Picking patches from git

```
git format-patch --start-number=N -k <commit>^...<commit>
```

# Building the package

Build the source package

```bash
debuild -S -sa -k$EMAIL
```

If you want to push to a PPA

```bash
cd ../
dput ppa:$LP_USER/$PPA_NAME $PROJECT_$NEW_VERSION-0ubuntu0.1_source.changes
```

# Debdiff of the changes

To create an Stable Release Update (SRU), you need a debdiff

```bash
debdiff old.dsc new.dsc > changes.debdiff
```

For security updates when it is an upstream release then .debdiff.gz should be used.

```bash
gzip changes.debdiff
```

Note that when trying to apply the debdiff, if any patches have been removed, they need to be reversed after `pull-lp-source` otherwise the debdiff won't apply eg with `patch --reverse -p1 < removed.patch`

# Patches

```
quilt push -a
quilt new debian/patches/my-super-change.patch
quilt add my-super-change
nano my-super-change
quilt refresh
quilt pop -a
```
