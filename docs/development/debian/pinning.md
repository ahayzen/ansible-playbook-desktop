
# Backports

## Packages

The following packages could be considered for using backports.

  * `cockpit*`
  * `firmware-linux`
  * `flatpak`
  * `syncthing`
  * `tlp*`
  * `xdg-desktop-portal`

If you need a newer kernel the following package should be considered.

  * `linux-image-amd64`

## Install

To install a package from backports use the following command.

`apt install -t buster-backports <package>`

## Apt Pinning

Simply add a file to `/etc/apt/preferences.d` to pin the relevant package to a backport release.

For example for cockpit could add the file `pin-backports-cockpit` with the following contents.

```
Package: cockpit*
Pin: release a=buster-backports
Pin-Priority: 500
```

# Testing / Sid

## Pinning Sid

When using Debian Testing one might want to allow for installing missing packages from Debian Sid.

After adding `sid` to your sources, the following could be added to a file in `/etc/apt/preferences.d`.

```
Package: *
Pin: release n=sid
Pin-Priority: 100
```

If a package exists in `testing` then one can use `apt install -t sid <package>` to install from `sid`.
