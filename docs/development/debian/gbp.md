# Install

```
sudo apt install git-buildpackage
```

# Create a new package from source

In an empty git direcotry, run the following command to import the source code.

```
gbp import-orig --debian-branch=debian/master --upstream-branch=upstream/latest --pristine-tar /path/to/tarball
```

# Configure

Run the following command to create a branch for debian packaging, then add your debian/ folder and commit it.

```
git checkout -b debian/master master
```

Also create a branch for gbp to track the latest code in.

```
git branch upstream/latest master
```

Example gbp config (we need network in pbuilder as we are using pip3)

```
[DEFAULT]
debian-branch = debian/master
upstream-branch = upstream/latest
upstream-tree = BRANCH
upstream-vcs-tag = v%(version)s

[buildpackage]
dist = sid
export-dir = build
# If you need network in pbuilder enable the line below
# pbuilder-options = "--use-network=yes"
```

# Update code

When a release has been tagged in the upstream code with the tar.gz url run the following command.

```
gbp import-orig <url>
```

This will update `debian/master`.

## Making a release

Update the changelog with new entries.

```
gbp dch
```

Then change `UNRELEASED` to `stable` and push the changes.

```
gbp tag
gbp push
```

# Native Build

Once you have a tag in the origin/master and debian/changelog that matches this version,
a native package can be build with the following command.

Note this command needs to be run from the `debian/*` branch.

```
gbp buildpackage
```

# pbuilder Building

Install pbuilder and qemu (for cross builds).

```
sudo apt install pbuilder qemu-user-static
```

Configure pbuilder to find apt depends.

```
echo "PBUILDERSATISFYDEPENDSCMD="/usr/lib/pbuilder/pbuilder-satisfydepends-apt" >> ~/.pbuilderrc
```

## Cross build for Raspbian

Add Raspbian keyring

```
sudo mkdir -p /usr/local/share/keyrings
wget https://archive.raspberrypi.org/debian/raspberrypi.gpg.key qO- | sudo gpg --import --no-default-keyring --keyring /usr/local/share/keyrings/raspbian-archive-keyring.gpg
```

Create raspbian buster armhf pbuilder env.

```
DIST=buster ARCH=armhf git-pbuilder create --debootstrapopts --keyring=/usr/local/share/keyrings/raspbian-archive-keyring.gpg --mirror=https://archive.raspbian.org/raspbian
```

Build the package in pbuilder.

```
gbp buildpackage --git-arch=armhf --git-pbuilder
```

## Native pbuilder

Create debian buster amd64 pbuilder env.

```
DIST=buster ARCH=amd64 git-pbuilder create
```

Build the package in pbuilder.

```
gbp buildpackage --git-arch=amd64 --git-pbuilder
```
