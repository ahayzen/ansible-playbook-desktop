pbuilder is useful for building debs in a clean environment to ensure that you have the correct build depends etc.

# Install

```
sudo apt install pbuilder
```

# Create the chroot

```
sudo pbuilder create --debootstrapopts --variant=buildd
```

If you want a different distro release use

```
sudo pbuilder create --debootstrapopts --variant=buildd --distribution bionic
```

If you want a i386 chroot then use

```
sudo pbuilder create --distribution focal --debootstrapopts --arch --debootstrapopts i386 --debootstrapopts --variant=buildd
```

# Enabling universe

Add universe to the chroot sources

```
echo """COMPONENTS="main restricted universe multiverse"
if [ "$DIST" == "squeeze" ]; then
    echo "Using a Debian pbuilder environment because DIST is $DIST"
    COMPONENTS="main contrib non-free"
fi""" | tee -a ~/.pbuilderrc
```

# Update the chroot

```
sudo pbuilder update --override-config
```

# Adding a PPA

Add the folllowing code to your pbuilder configuration to add the stable-phone-overlay PPA.

```
ALLOWUNTRUSTED=yes
OTHERMIRROR="deb http://ppa.launchpad.net/ci-train-ppa-service/stable-phone-overlay/ubuntu xenial main"
```

Note: Currently setting untrusted to true, should instead use APTKEYRINGS or DEBOOTSTRAPOPTS ?

# Building

```
sudo pbuilder --build *.dsc
```

If you are using a different distro use

```
sudo pbuilder --build *.dsc --distribution bionic
```

# Untrusted packages

If you are using PPAs or have untrusted packages use the following build command.

```
sudo pbuilder --allow-untrusted --build *.dsc
```

# pbuilder-dist

Create an i386 environment.

```
pbuilder-dist focal i386 create
```

Build a package in an existing environment.

```
pbuilder-dist focal i386 build *.dsc
```
