# Pull existing sources

```bash
pull-lp-source $PROJECT $UBUNTU_SERIES
pull-debian-source $PROJECT $DEBIAN_SERIES
```

# Rearrange the folders

Move Debian folder as we'll replace it with the Ubuntu one, but want to keep for comparing.

```bash
mv $PROJECT-$DEBIAN_VERSION/ $PROJECT-$DEBIAN_VERSION-debian/
```

Copy existing Ubuntu folder to the version of Debian,
as we are going to sync the changes to the Debain version.

```bash
cp -R $PROJECT-$UBUNTU_VERSION/ $PROJECT-$DEBIAN_VERSION/
```

# Compare differences

Compare the differences that have happened in Debian and compare them to our Ubuntu version.

Pick the upstream changes that should be carried in the Ubuntu version.

```bash
meld $PROJECT-$DEBIAN_VERSION-debian/ $PROJECT-$DEBIAN_VERSION/
```

# Build

```bash
debuild -S -sa -k$EMAIL
```
