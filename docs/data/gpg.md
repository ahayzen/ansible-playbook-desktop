# Generation

## Master Key

```bash
$ gpg --gen-key
> 4 (RSA sign only)
> 4096 bits
> 0 does not expire
```

## Revocation Certificate

```bash
$ gpg --output revocation-cert.txt --gen-revoke ABCDEF01
> 1 (key has been comprimised)
```

## Sub keys

This allows for having one key for signing, encrypting, and authenticating.
Which is useful for a YubiKey.

```bash
$ gpg --expert --edit-key ABCDEF01
> addkey
4 (RSA sign only)
4096 bits
0 does not expire
> addkey
6 (RSA encrypt only)
4096 bits
0 does not expire
> addkey
8 (RSA set your own caps)
s e a q  (enable authentication)
4096 bits
0 does not expire
```

# List Keys

```bash
$ gpg --list-keys
$ gpg --list-secret-keys
```

# Export

## GPG Keys

```bash
$ gpg --output mykey_pub.gpg --armor --export ABCDEF01
$ gpg --output mykey_sec.gpg --armor --export-secret-key ABCDEF01
$ gpg --output mykey_sub_sec.gpg --armor --export-secret-subkeys ABCDEF01
```

## Paperkey

```bash
$ gpg --output mykey_sec_noarmor.gpg --dearmor mykey_sec.gpg
$ paperkey --secret-key mykey_sec_noarmor.gpg --output mykey_sec.txt
```


# Import

## GPG Keys

```bash
$ gpg --import mykey_pub.gpg
$ gpg --allow-secret-key-import --import mykey_sec.gpg
$ gpg --edit-key ABCDEF01
> trust
5 = I trust ultimately
```

Note for a key with subkeys you should just be able to import the main public key
and then see `sec#` and `ssb` when using `gpg --list-secret-keys ABCDEF01`.

## Paperkey

Use the following command to convert a paperkey into a gpg secret key,
which can then be imported using the command above.

```bash
$ paperkey --pubring mykey_pub.gpg --secrets mykey_sec.txt --output mykey_sec.gpg
```

# SmartCard

## Import

With a smartcard you only need to import the public key and then if the smartcard is plugged in the secret keys can be automatically detected after running `gpg --card-status`.

```bash
$ gpg --import mykey_pub.gpg
$ gpg --card-status
$ gpg --list-keys
```

## Moving subkeys

Ensure that you have exported a backup of the master and subkeys,
as this will remove the private parts in your gpg keyring.

```bash
$ gpg --edit-key ABCDEF01
> toggle
> key 1
> keytocard
1 signature key
> key 1

> key 2
> keytocard
2 encryption key
> key 2

> key 3
> keytocard
3 auth key

> save
```
