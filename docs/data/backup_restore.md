
# Tasks

  * Setup syncthing folders
  * Copy legacy data
  * Copy project data
  * Git clone password store

# Folders

| Purpose | Path |
|---------|------|
| Network keys | `/etc/NetworkManager/system-connections` |
| PGP keys | `~/.gnupg` |
| Password-store | `~/.password-store` |
| SSH keys | `~/.ssh` |
| HexChat | `~/.var/app/io.hexchat.HexChat` |
| Game Save Folders | [paths](/docs/config/games.md) |
