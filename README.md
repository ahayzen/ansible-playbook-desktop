
This configures a machine have the GNOME Desktop with the set of applications and configurations I prefer.
Note that it may overwrite any pre-existing data or configuration and is intended to be run after installation.

It contains a script (`deploy.py`) which asks basic questions about the type of deployment that you want and is able to deploy to a local or remote (over SSH) machine.

The script generates a `playbook.yml` which it then uses to deploy the target machine.

# Target

The script is designed to be run on the latest version of the following distributions

  * Debian Stable (netinstall)
  * Ubuntu LTS

# Usage

Ensure that the machine running the script and the target machine (which can be the same machine) has `python3` installed.

Then simply run the following command and follow the instructions.

```bash
$ ./deploy.py
```

Once completed there is a list of [configuration tasks](/docs/config/manual.md) of things that aren't configured automatically.

# Roles

Note some roles have a user variant, this is for configuration that is stored in the user profile (eg `dconf` settings or data in `/home/user`). As these are separate roles if you have multiple users you can change the `username` global var and re-run just these roles.

| Role                     | Purpose                                       |
| ------------------------ | --------------------------------------------- |
| base-session             | Services and general system configuration     |
| base-session-user        | ^^ user config                                |
| common-apps              | Firefox, LibreOffice, Telegram                |
| crypto-backup-user       | LUKS header backup                            |
| developer-additions      | byobu, IDEs, IRC etc                          |
| developer-additions-user | ^^ user config                                |
| games-apps               | 0 A.D., Steam etc                             |
| gnome-apps               | GNOME desktop related apps                    |
| gnome-apps-user          | ^^ user config                                |
| gnome-session            | GNOME desktop environment                     |
| gnome-session-user       | ^^ user config                                |
| graphic-apps             | GIMP, Inkscape etc                            |
| graphic-apps-user        | ^^ user config                                |
| laptop-additions         | TLP etc                                       |
| media-apps               | Spotify, VLC etc                              |
| media-apps-user          | ^^ user config                                |
| scanner-brother-brscan4  | Brother Scanner Sane Driver (brscan4)         |

# Variables

| Variable                 | Purpose                                                   |
| ------------------------ | --------------------------------------------------------- |
| username                 | Username for any -user roles or permissions               |
| brother_brscan4_ipaddr   | IP Address of the Brother Scanner                         |
| brother_brscan4_name     | Model name of the Brother Scanner                         |
| developer_user_email     | Email address of user (eg git, deb packaging)             |
| developer_user_name      | Name of user (eg git, deb packaging)                      |
| games_extra              | If larger games should be instaled                        |
