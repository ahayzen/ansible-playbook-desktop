#!/usr/bin/env python3

# Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from ansible.module_utils.basic import AnsibleModule
from os import path


def app_exists(app, module):
    return path.exists("/snap/%s" % app)


def install_app(app, module):
    if not app_exists(app, module):
        if module.check_mode:
            return True

        cmd = ["snap", "install"]

        if module.params["classic"] is True:
            cmd += ["--classic"]

        cmd += [app]

        rc, stdout, stderr = module.run_command(cmd)
        if rc == 0:
            return True
        else:
            module.fail_json(msg="Could not install snap: %s" % stderr)
            return False
    else:
        return False


def remove_app(app, module):
    if app_exists(app, module):
        if module.check_mode:
            return True

        cmd = ["snap", "remove"]

        if module.params["purge"] is True:
            cmd += ["--purge"]

        cmd += [app]

        rc, stdout, stderr = module.run_command(cmd)
        if rc == 0:
            return True
        else:
            module.fail_json(msg="Could not remove snap: %s" % stderr)
            return False
    else:
        return False


def run_module():
    module_args = {
        "app": {
            "required": True,
            "type": "list",
        },
        "classic": {
            "default": False,
            "required": False,
            "type": "bool",
        },
        "purge": {
            "default": False,
            "required": False,
            "type": "bool",
        },
        "state": {
            "choices": ["absent", "present"],
            "default": "present",
            "required": False,
            "type": "str",
        },
        # TODO: connections for configure ?
    }

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    apps = module.params["app"]
    changed = False
    state = module.params["state"]

    for app in apps:
        if state == "absent":
            changed |= remove_app(app, module)
        elif state == "present":
            changed |= install_app(app, module)

    module.exit_json(changed=changed)


def main():
    run_module()


if __name__ == '__main__':
    main()
