#!/usr/bin/env python3

# Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from ansible.module_utils.basic import AnsibleModule
from os import mkdir, path


def build_package(name, module):
    if module.check_mode:
        return True

    dir, file = make_paths(name)
    cmd = "equivs-build %s" % file
    rc, _, stderr = module.run_command(cmd, cwd=dir)

    if rc == 0:
        return True
    else:
        module.fail_json(msg="Could not build equivs package %s: %s" %
                         (name, stderr))
        return False


def install_package(name, module):
    if module.check_mode:
        return

    _, file = make_paths(name)
    cmd = "apt install --yes %s_1.0.0_all.deb" % file
    rc, _, stderr = module.run_command(cmd)

    if rc == 0:
        return True
    else:
        module.fail_json(msg="Could not apt install package %s: %s" %
                         (name, stderr))
        return False


def make_package_string(description, provides, section, version):
    provides_version = provides if version == "" else "%s (%s)" % (provides,
                                                                   version)
    return """Section: {section}
Priority: optional
Standards-Version: 3.9.8.0

Package: fake-package-{provides}
Version: 1.0.0
Maintainer: Andrew Hayzen <ahayzen@gmail.com>
Provides: {provides_version}
Architecture: all
Description: Fake package
 This is a fake package to let the packaging system
 believe that this Debian package is installed.
 .
 {description}""".format(description=description,
                         provides=provides,
                         provides_version=provides_version,
                         section=section)


def make_paths(name):
    name = "fake-package-%s" % name
    dir = path.join(path.sep, "opt", name)
    file = path.join(dir, name)

    return (dir, file)


def write_package_file(name, data, module):
    dir, file = make_paths(name)

    # Check if the file exists and is the same
    if path.exists(file):
        with open(file, "r") as f:
            if f.read() == data:
                return False

    # If we are in check mode
    if module.check_mode:
        return True

    # Ensure the directory exists
    if not path.exists(dir):
        mkdir(dir)

    # Write the new contents
    with open(file, "w") as f:
        f.write(data)

    return True


def run_module():
    module_args = {
        "description": {
            "default": "",
            "required": False,
            "type": "str",
        },
        "provides": {
            "required": True,
            "type": "str",
        },
        "section": {
            # FIXME: a default section?
            "default": "misc",
            "required": False,
            "type": "str",
        },
        "version": {
            "default": "",
            "required": False,
            "type": "str",
        }
    }

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    description = module.params["description"]
    provides = module.params["provides"]
    section = module.params["section"]
    version = module.params["version"]

    name = provides

    data = make_package_string(description, provides, section, version)
    changed = write_package_file(name, data, module)

    if changed:
        build_package(name, module)
        install_package(name, module)

    module.exit_json(changed=changed)


def main():
    run_module()


if __name__ == "__main__":
    main()
