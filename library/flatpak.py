#!/usr/bin/env python3

# Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from ansible.module_utils.basic import AnsibleModule
from os import path


def app_exists(app, module):
    base = path.expanduser("~/.local/share/flatpak" if module.params["target"]
                           == "user" else "/var/lib/flatpak")
    return path.exists(path.join(base, "app/%s" % app)) or path.exists(
        path.join(base, "runtime/%s" % app))


def override_path(app, module):
    if module.params["target"] == "user":
        return path.expanduser("~/.local/share/flatpak/overrides/%s" % app)
    else:
        return "/var/lib/flatpak/overrides/%s" % app


def target_as_cmd(module):
    if module.params["target"] == "user":
        return ["--user"]
    else:
        return ["--system"]


def configure_app(app, override, module):
    if app_exists(app, module):
        if module.check_mode:
            return True

        exists = path.exists(override_path(app, module))
        cmd = ["flatpak", "override"]

        cmd += target_as_cmd(module)
        cmd += [override, app]
        rc, stdout, stderr = module.run_command(cmd)

        if rc == 0:
            # Override always changes the file, so for now only report changed
            # if we create the file
            return not exists
        else:
            module.fail_json(msg="Could not configure flatpak: %s" % stderr)
            return False
    else:
        module.fail_json(
            msg="Could not configure flatpak (app missing): %s" % app)
        return False


def install_app(app, module):
    if not app_exists(app, module):
        if module.check_mode:
            return True

        cmd = ["flatpak", "install", "-y"]
        remote = module.params["remote"]

        cmd += target_as_cmd(module)
        cmd += [remote, app]

        rc, stdout, stderr = module.run_command(cmd)

        if rc == 0:
            return True
        else:
            module.fail_json(msg="Could not install flatpak: %s" % stderr)
            return False
    else:
        return False


def remove_app(app, module):
    if app_exists(app, module):
        if module.check_mode:
            return True

        rc, stdout, stderr = module.run_command(
            ["flatpak", "uninstall", "-y", app])

        if rc == 0:
            return True
        else:
            module.fail_json(msg="Could not remove flatpak: %s" % stderr)
            return False
    else:
        return False


def run_module():
    module_args = {
        "app": {
            "required": True,
            "type": "list",
        },
        "override": {
            "default": [],
            "required": False,
            "type": "list",
        },
        "remote": {
            "default": "flathub",
            "required": False,
            "type": "str",
        },
        "state": {
            "choices": ["absent", "configure", "present"],
            "default": "present",
            "required": False,
            "type": "str",
        },
        "target": {
            "choices": ["system", "user"],
            "default": "system",
            "required": False,
            "type": "str"
        },
    }

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    apps = module.params["app"]
    changed = False
    state = module.params["state"]

    for app in apps:
        if state == "absent":
            changed |= remove_app(app, module)
        elif state == "configure":
            for override in module.params["override"]:
                changed |= configure_app(app, override, module)
        elif state == "present":
            changed |= install_app(app, module)

    module.exit_json(changed=changed)


def main():
    run_module()


if __name__ == '__main__':
    main()
