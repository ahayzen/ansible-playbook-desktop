#!/usr/bin/env python3

from os import path
from subprocess import PIPE, Popen, STDOUT

BASE_PLAYBOOK: str = """
---

- name: deploy user machine
  become: yes
  hosts: all
"""

BASE_ROLES: list = [
    ("base-session", "base-session-user"),
    ("common-apps", None),
    ("gnome-session", "gnome-session-user"),
    ("gnome-apps", "gnome-apps-user"),
]

OPTIONAL_ROLES_WITH_PARAMS: list = [
    (None, "crypto-backup-user", "Is the target using LUKS encryption?", {}),
    ("laptop-additions", None, "Is the target a laptop?", {}),
    ("developer-additions", "developer-additions-user",
     "Is the target for a developer?", {
         "developer_user_name": "Developer User Name (eg git):",
         "developer_user_email": "Developer User Email (eg git):"
     }),
    ("media-apps", "media-apps-user", "Install media apps? (eg Spotify, VLC)",
     {}),
    ("graphic-apps", "graphic-apps-user",
     "Install graphic apps? (eg GIMP, Inkscape)", {}),
    ("game-apps", None, "Install games? (eg Steam, Sudoku)", {
        "games_extra":
        "Install extra games (eg 0 A.D., PlayOnLinux, SuperTuxKart) (y/n):",
    }),
    ("scanner-brother-brscan4", None,
     "Install Brother Scanner (brscan4) drivers?", {
         "brother_brscan4_name": "Scanner model (eg DCP-L8400CDN):",
         "brother_brscan4_ipaddr": "Scanner IP Address:"
     })
]

CHOICE_VARS: list = {
    # Packaging choice doesn't exist anymore
    # but leave as an example of Python code
    #
    # "packaging": [
    #     ("Classic packaging", "classic"),
    #     ("Modern packaging (flatpak / snap)", "modern"),
    # ]
}

PLAYBOOK: str = "playbook.yml"


def check_command_available(command: str, test_param="--version") -> bool:
    print("Checking for command: '%s': " % command, end="")

    try:
        if run_command([command, test_param], stderr=STDOUT).returncode != 0:
            print("missing")
            return False
        else:
            print("OK")
            return True
    except FileNotFoundError:
        print("missing")
        return False


def check_package_installed(package: str) -> bool:
    print("Checking for package `%s`: " % package, end="")

    if run_command(["dpkg", "--status", package],
                   stderr=STDOUT).returncode != 0:
        print("missing")
        install_cmd = ["sudo", "apt-get", "install", "-y", package]
        print("> $", *install_cmd)
        return run_command(install_cmd, stdout=None).returncode == 0
    else:
        print("OK")
        return True


def remove_package(package: str):
    print("Tidying package: '%s'" % package)
    remove_cmd = ["sudo", "apt-get", "purge", "--autoremove", "-y", package]
    print("> $", *remove_cmd)
    return run_command(remove_cmd, stdout=None).returncode == 0


def input_choice(entries: list) -> object:
    question: str = "Select an option\n"

    for (i, (text, _)) in enumerate(entries):
        question += " - %d: %s\n" % (i, text)

    question += "Option [0-%d]:" % (len(entries) - 1)

    try:
        index: int = int(input_strip(question))
        if index > -1 and index < len(entries):
            return entries[index][1]
        else:
            raise ValueError
    except ValueError:
        print("You must enter a valid value.\n")
        return input_choice(entries)


def input_strip(question: str) -> str:
    return input("%s " % question).strip()


def input_yn(question: str) -> bool:
    return input_strip("%s (y/n):" % question).lower() == "y"


def push_system_user_or_both(system: str, user: str, mode: int) -> str:
    lines: str = ""

    if system is not None and EnumMode.SYSTEM in mode:
        lines += "    - %s\n" % system

    if user is not None and EnumMode.USER in mode:
        lines += "    - %s\n" % user

    return lines


def run_command(cmd: list, stdout=PIPE, **kwargs) -> object:
    pid = Popen(cmd, stdout=stdout, **kwargs)
    try:
        pid.wait()
    except KeyboardInterrupt as e:
        pid.kill()
        raise e
    finally:
        return pid


class EnumMode(object):
    SYSTEM: int = 2
    USER: int = 3


def generate_playbook(filename: str):
    # TODO: mention that system mode needs to be run at least once
    # on the system or change the question to be based
    mode_str: str = input_strip("Deploy mode (user,system,both):")
    if mode_str == "user":
        mode: int = [EnumMode.USER]
    elif mode_str == "system":
        mode: int = [EnumMode.SYSTEM]
    else:
        mode: int = [EnumMode.SYSTEM, EnumMode.USER]

    #
    # Set up global vars
    #
    playbook_vars: str = "  vars:\n"

    if EnumMode.USER in mode:
        target_user: str = input_strip("> Target username:")
        playbook_vars += "    username: %s\n" % target_user

    for (key, value) in CHOICE_VARS.items():
        playbook_vars += "    %s: %s\n" % (key, input_choice(value))

    #
    # Set up roles
    #
    playbook_roles: str = "  roles:\n"

    # Base roles which don't have config
    for (system, user) in BASE_ROLES:
        playbook_roles += push_system_user_or_both(system, user, mode)

    # Optional roles with questions
    for (system, user, question, params) in OPTIONAL_ROLES_WITH_PARAMS:
        # Check if it is a system question we have system mode
        system_question: bool = system is not None and EnumMode.SYSTEM in mode
        # Check if it is a user question we have user mode
        user_question: bool = user is not None and EnumMode.USER in mode

        # If one of the modes is valid to ask a question then ask it
        if (system_question or user_question) and input_yn(question):
            playbook_roles += push_system_user_or_both(system, user, mode)

            # Ask user questions from parameters and set to variables
            for (k, v) in params.items():
                if v.endswith(" (y/n):"):
                    value = "yes" if input_yn("> %s" % v[:-7]) else "no"
                else:
                    value = input_strip("> %s" % v)

                playbook_vars += "    %s: %s\n" % (k, value)

    #
    # Write playbook
    #
    with open(filename, "w") as f:
        f.write(BASE_PLAYBOOK + playbook_vars + playbook_roles)


def deploy(filename: str):
    cmd: list = [
        "ansible-playbook", "--ask-pass", "--ask-become-pass", "-e",
        "ansible_python_interpreter=/usr/bin/python3"
    ]

    # Local or remote ?
    if input_yn("Is the target localhost?"):
        cmd += ["-i", "localhost,", "-c", "local"]
    else:
        ip_addr: str = input_strip("> IP Addr of the target:")
        ssh_user: str = input_strip("> SSH username:")

        cmd += [
            "-i",
            "%s," % ip_addr, "-e",
            "ansible_remote_user=%s" % ssh_user
        ]

    # Add playbook filename
    cmd += [filename]

    # Check if we should exec
    print("")
    print(" ".join(cmd))

    if input_yn("> OK to execute above command?"):
        run_command(cmd, stdout=None)


def main(filename: str):
    print("""Welcome to my automatic provision system for desktop !
Please ensure you have read and understand ALL roles before using this script.

This script is designed to be run on Debian Stable or Ubuntu LTS.
It assumes that `python3` is installed on this machine and the
target machine (which could also be this machine).
""")

    # Check for ansible-playbook command (we might have nixpkg)
    #
    # If it's missing then install apt package
    if not check_command_available("ansible-playbook"):
        check_package_installed("ansible")

    # We need python3-psutil to use the ansible dconf module
    # https://docs.ansible.com/ansible/latest/modules/dconf_module.html#notes
    check_package_installed("python3-psutil")

    print("")

    if (not path.exists(filename)
            or input_yn("%s exists, regenerate playbook?" % filename)):
        print("Generating playbook ...")
        generate_playbook(filename)
        print("... Generated %s" % filename)

    print("")

    if input_yn("Do you want to deploy %s?" % filename):
        deploy(filename)

    remove_package("ansible")


if __name__ == "__main__":
    try:
        main(PLAYBOOK)
    except KeyboardInterrupt:
        print("Exiting ...")
