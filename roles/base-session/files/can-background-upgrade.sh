#!/usr/bin/env bash

# Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

#
# External device checks
#

#
# AC Power check
#
# on_ac_power exits with 1 when using battery (0 on AC and 255 on unknown)
on_ac_power
if [ $? -eq 1 ]; then
    echo "Battery in use, exiting."
    exit 1;
fi

# Microphone check
#
# Loop through asound capture cards, if closed is not in the status then grep will exit 0
for path in /proc/asound/card*/pcm*c/sub*/status; do
    if ! grep -q closed "$path"; then
        echo "Microphone in use, exiting."
        exit 1;
    fi
done


# Video camera check
#
# If any webcams are in use, fuser will exit 0
if fuser --silent /dev/video*; then
    echo "Camera in use, exiting."
    exit 1;
fi



#
# Networking checks
#

# Metered or no network check
#
# If network is metered or connection is not full python exits 0
#
# Note we need python3-gi installed for the import to work, this should come from software-properties-common
if /usr/bin/env python3 -c "from gi.repository.Gio import NetworkConnectivity as NC, NetworkMonitor as NM; exit(NM.get_connectivity(NM.get_default()) == NC.FULL and not NM.get_network_metered(NM.get_default()))"; then
    echo "Network is metered or not connected, exiting."
    exit 1;
fi

# VPN check
#
# Loop through VPN connection types from NetworkManager
# and find any that have never default set to "no", which means that the
# VPN could be the default route for all traffic
#
# Note that we do not inspect connection of the type tunnel.
#
# List all connections where VPN is the type
mapfile -t VPNS < <(nmcli c show --active | awk 'NR>1 && $3 == "vpn" { print $2 }')
for VPN in "${VPNS[@]}"; do
    # Retrieve the info of the connection
    VPN_SHOW=$(nmcli c show "$VPN")
    # Extract the method and never default properties
    IPV4_METHOD=$(echo "$VPN_SHOW" | awk '$1 == "ipv4.method:" { print $2 }')
    IPV4_NEVER_DEFAULT=$(echo "$VPN_SHOW" | awk '$1 == "ipv4.never-default:" { print $2 }')
    IPV6_METHOD=$(echo "$VPN_SHOW" | awk '$1 == "ipv6.method:" { print $2 }')
    IPV6_NEVER_DEFAULT=$(echo "$VPN_SHOW" | awk '$1 == "ipv6.never-default:" { print $2 }')

    # If the method is not ignore and the never default is no, then it means all traffic is potentially going through the VPN
    if [[ ("$IPV4_METHOD" != 'ignore' && "$IPV4_NEVER_DEFAULT" == 'no') || ("$IPV6_METHOD" != 'ignore' && "$IPV6_NEVER_DEFAULT" == 'no') ]]; then
        echo "VPN with all routes enabled, exiting."
        exit 1;
    fi
done



#
# Service checks
#

# KVM check
#
# If kvm is in use, fuser will exit 0
if fuser --silent /dev/kvm; then
    echo "KVM in use, exiting."
    exit 1;
fi



#
# Window Manager checks
#

# Fullscreen window check
#
# Loop through open windows and if _NET_WM_STATE_FULLSCREEN is set grep exits 0
mapfile -t SESSION_ARRAY < <( loginctl --no-legend --no-pager )

for SESSION in "${SESSION_ARRAY[@]}"; do
    SESSION_ID=$(echo "$SESSION" | awk '{print $1}')
    USER_ID=$(echo "$SESSION" | awk '{print $2}')
    USER_NAME=$(echo "$SESSION" | awk '{print $3}')

    # Extract and check the session type for the session id
    SESSION_TYPE=$(loginctl show-session "$SESSION_ID" -p Type --value)

    # If we are using GNOME then query using shell command
    if echo "$XDG_CURRENT_DESKTOP" | grep -iq gnome; then
        GNOME_SHELL_FULLSCREEN=$(sudo --user="$USER_NAME" bash -c "
            export DBUS_SESSION_BUS_ADDRESS=\"unix:path=/run/user/$USER_ID/bus\"

            dbus-send \
                --session \
                --type=method_call \
                --print-reply \
                --dest=org.gnome.Shell \
                /org/gnome/Shell \
                org.gnome.Shell.Eval \
                string:\"var windows = global.get_window_actors().filter(actor => { return actor.metaWindow.fullscreen; }); if (windows.length > 0) { 'found'; } else { 'missing'; }\"
        ")

        if echo "$GNOME_SHELL_FULLSCREEN" | grep -q found; then
            echo "Found fullscreen window, exiting."
            exit 1;
        fi
    # If we are X11 then use wmctrl
    elif [[ $SESSION_TYPE == 'x11' ]]; then
        # Who command lists the display id's for usernames
        mapfile -t WHO_ARRAY < <( who | awk -v name="$USER_NAME" '$1 == name {print $2}' | sort -u )

        for WHO_LINE in "${WHO_ARRAY[@]}"; do
            # Ensure the line is a valid display (and not pts)
            if [[ $WHO_LINE == *':'* ]]; then
                export DISPLAY=$WHO_LINE
                export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$USER_ID/bus"
                export XAUTHORITY="/run/user/$USER_ID/gdm/Xauthority"

                # Find windows in x11 and check if they are fullscreen
                for winid in $(wmctrl -l | awk '{print $1}'); do
                    if xprop -id "$winid" | grep -q _NET_WM_STATE_FULLSCREEN; then
                        echo "Found fullscreen window, exiting."
                        exit 1;
                    fi
                done
            fi
        done
    else
        echo "Using Wayland on unsupported desktop environment, assuming no fullscreen windows."
    fi
done
