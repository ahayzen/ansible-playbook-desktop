#!/usr/bin/env python3
#
# Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
from gi.repository import GLib
from dbus import Interface, SystemBus
from dbus.exceptions import DBusException
from dbus.types import UnixFd
import notify2
from tempfile import NamedTemporaryFile
from urllib.request import urlopen

MAIN_LOOP = GLib.MainLoop()


class FwupdDBus(object):
    def __init__(self):
        self.system_bus = SystemBus()
        self.proxy = self.system_bus.get_object("org.freedesktop.fwupd", "/")

        self.interface = Interface(self.proxy,
                                   dbus_interface="org.freedesktop.fwupd")

    def devices(self):
        return [device["DeviceId"] for device in self.interface.GetDevices()]

    def list_upgrades(self):
        updates = []

        for device_id in self.devices():
            try:
                updates += self.interface.GetUpgrades(device_id)
            except DBusException as e:
                dbus_name = e.get_dbus_name()
                if dbus_name in ("org.freedesktop.fwupd.InvalidFile",
                                 "org.freedesktop.fwupd.NotSupported",
                                 "org.freedesktop.fwupd.NothingToDo"):
                    continue
                else:
                    raise e

        return updates

    def refresh(self):
        for remote in self.interface.GetRemotes():
            if remote["Enabled"] and "RemoteId" in remote and "Uri" in remote:
                uri = remote["Uri"]
                remote_id = remote["RemoteId"]
                data = urlopen(uri)
                sig = urlopen(uri + ".asc")

                if data.status == 200 and sig.status == 200:
                    self.update_metadata(remote_id, data, sig)

    def update_metadata(self, remote, data, sig):
        data_fd = NamedTemporaryFile()
        data_fd.write(data.read())
        data_fd.seek(0)

        sig_fd = NamedTemporaryFile()
        sig_fd.write(sig.read())
        sig_fd.seek(0)

        self.interface.UpdateMetadata(remote, UnixFd(data_fd), UnixFd(sig_fd))

        data_fd.close()
        sig_fd.close()


def callback_closed(notification, action=None):
    print("Firmware update available notification closed")
    MAIN_LOOP.quit()


def show_notification():
    # Create a reboot required notification
    notification = notify2.Notification(
        summary="A new system firmware upgrade is available",
        message="Consult your system administrator to perform the upgrade.",
        icon="software-update-urgent",
    )
    # Create a reboot action and listen for when the notificaiton closes
    notification.add_action("ok", "OK", callback_closed)
    notification.connect("closed", callback_closed)
    # Set the urgency to critical so that it is a persistent notification
    notification.set_urgency(notify2.URGENCY_CRITICAL)
    # Show the notification
    notification.show()

    print("Firmware update available notification shown")


if __name__ == "__main__":
    fwupd = FwupdDBus()
    # Ensure metadata has been refreshed
    try:
        fwupd.refresh()
    except DBusException as e:
        print("DBus error when refreshing, "
              "ignore as we can still check for updates: %s" % e)

    # Check if there are any updates available
    if len(fwupd.list_upgrades()) > 0:
        # Init notify2 with glib and show notification
        notify2.init("com.ahayzen.fwupd-notifier-gui", mainloop="glib")
        show_notification()

        # Run GLib main loop so that we can receive callbacks
        MAIN_LOOP.run()
