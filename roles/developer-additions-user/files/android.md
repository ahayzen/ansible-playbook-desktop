# Android SDK

Download and Install the Android Command Line Tools
https://developer.android.com/studio/#command-tools

```
sdkmanager --sdk_root=$HOME/Downloads/godot/androidsdk "platform-tools" "build-tools;30.0.3" "platforms;android-29" "cmdline-tools;latest" "cmake;3.10.2.4988404" "ndk;21.4.7075529"
```

This installs the relevant packages for Godot.

# Java and Keytool

Ensure your `~/.config/nixpkgs/developer.nix` contains `pkgs.jdk11_headless`.

```nix
{ config, home, pkgs, ... }:

{
  home.packages = [
    pkgs.jdk11_headless  # for Godot Android (keytool)
  ];
}
```

Then use keytool

```bash
$ mkdir ~/.android
$ cd ~/.android
$ keytool -keyalg RSA -genkeypair -alias androiddebugkey -keypass android -keystore debug.keystore -storepass android -dname "CN=Android Debug,O=Android,C=US" -validity 9999
```

# Configure

In Godot Editor Settings -> Android configure the following values.

| Key | Linux | Windows |
|-----|-------|---------|
| Android SDK Path | `<android_sdk_path>` | `<android_sdk_path>` |
| keystore | `/home/USER/.android/debug.keystore` | `debug.keystore` |

Also ensure that you have export templates downloaded in Editor -> Manage Export Templates

# Further reading

  * https://docs.godotengine.org/en/stable/getting_started/workflow/export/exporting_for_android.html
